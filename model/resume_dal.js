var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 SELECT * FROM resume;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM resume;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'SELECT r.*, a.first_name, a.last_name ' +
        'FROM resume r LEFT JOIN account a on r.account_id = a.account_id ' +
        'WHERE r.resume_id = ?;'

    // TODO: Which query? Is this my problem?

    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT RESUME
    var query = 'INSERT INTO resume (resume_name) VALUES (?)';

    var queryData = [params.resume_name];

    connection.query(query, params.resume_name, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS resume_getinfo;

 DELIMITER //
 CREATE PROCEDURE resume_getinfo(_resume_id int)
 BEGIN
 SELECT l.* FROM resume l WHERE l.resume_id = _resume_id;
 END; //
 DELIMITER ;

     # Call the Stored Procedure
     CALL resume_getinfo (4);

 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};